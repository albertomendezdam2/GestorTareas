package com.alberto.tareas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int eleccion;
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			// TODO: handle exception
			ex.getMessage();
		}
		
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/tareas", "root", "");
			GestorTareas control = new GestorTareas(conn);
			Statement stat = conn.createStatement();
			
			do {
				System.out.println("Elija una operacion\n1. A�adir tarea \n2. Eliminar tarea\n3. Alta detalle"
						+ " \n4. Listado tareas \n5. Buscar tarea \n6. Salir");
				eleccion = scan.nextInt();
				scan.nextLine();
				switch (eleccion) {
				case 1:
					System.out.println("Nombre: ");
					String ref = scan.nextLine();
					//System.out.println("Id: ");
					int cant = 0;
					Tareas t = new Tareas(ref);
					control.altaTarea(t);
					break;
				case 2:
					Tareas tar = new Tareas();
					control.consultarTareas();
					System.out.println("ID tarea: ");
					cant = scan.nextInt();
					tar.setIdTarea(cant);
					control.eliminarTarea(tar);

					// }while (1 != 0);
					break;
				case 3:
					Detalle det = new Detalle();
					System.out.println("ID Tarea: ");
					int id = scan.nextInt();
					det.setIdTarea(id);
					scan.nextLine();
					System.out.println("Nombre Tarea: ");
					String nDet = scan.nextLine();
					det.setNombreDetalle(nDet);
					String rel = "";
					do {
						System.out.println("Realizado S/N: ");
						rel = scan.nextLine();
					}while(!rel.equals("S") && !rel.equals("N"));
					
					if(rel.equals("S")) {
						det.setRealizado(true);
					}else {
						det.setRealizado(false);
					}
					control.altaDetalle(det);
					break;
				case 4:
					System.out.println("================================\n"
							+ "TAREAS\n================================ ");
					control.consultarTareas();
					break;
				case 5:
					System.out.println("Nombre de la tarea: ");
					String nombreTar = scan.nextLine();
					tar = new Tareas();
					tar.setNombre(nombreTar);
					control.consultaTarea(tar);
				default:
					//System.out.println("Opci�n no v�lida");
					break;
				}
			} while (eleccion != 6);
			scan.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package com.alberto.tareas;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Tareas {
	private int idTarea;
	private String nombre;
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	ArrayList<Detalle> detalles = new ArrayList<>();
	Detalle detalle = new Detalle();
	public Tareas(int id, String nombre) {
		super();
		this.idTarea = id;
		this.nombre = nombre;
	}
	
	
	
	public Tareas(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Tareas() {
		super();		
	}
	
	public Tareas(Connection conn, int idTarea) {}


	public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}

	@Override
	public String toString() {
		return "Tareas --> idTarea: " + idTarea + "\tNombre:" + nombre;
	}
	
	
}

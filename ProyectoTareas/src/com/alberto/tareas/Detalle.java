package com.alberto.tareas;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Detalle {

	
	private Connection conn;
	private int id, idTarea;
	private String nombreDetalle;
	private boolean realizado;
	
	public Detalle() {
		super();
	}

	public Detalle(int id, int idTarea, String nombreDetalle, boolean realizado) {
		super();
		this.id = id;
		this.idTarea = idTarea;
		this.nombreDetalle = nombreDetalle;
		this.realizado = realizado;
	}
	
	public Detalle(Connection conn, int id, int idTarea, String nombreDetalle, boolean realizado) {
		super();
		this.conn = conn;
		this.id = id;
		this.idTarea = idTarea;
		this.nombreDetalle = nombreDetalle;
		this.realizado = realizado;
	}

	
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdTarea() {
		return idTarea;
	}

	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}

	public String getNombreDetalle() {
		return nombreDetalle;
	}

	public void setNombreDetalle(String nombreDetalle) {
		this.nombreDetalle = nombreDetalle;
	}

	public boolean isRealizado() {
		return realizado;
	}

	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}

	@Override
	public String toString() {
		String cadena = "Detalle --> Id: " + id + ", IdTarea: " + idTarea + ", Detalles:" + nombreDetalle + ", ";
		if(realizado) {
			cadena += " Realizado: S�";
		}else {
			cadena += " Realizado: No";
		}
		return cadena;
	}
	
	
}

package com.alberto.tareas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GestorTareas {
	Tareas tareas = new Tareas();
	Detalle detalle = new Detalle();
	Connection conn;

	public GestorTareas(Connection conn) {
		// TODO Auto-generated constructor stub
		this.conn = conn;
	}

	public void altaTarea(Tareas t) {
		
			try {
				Statement stat = this.conn.createStatement();
				stat.executeUpdate("INSERT INTO tarea VALUES(NULL, '" + t.getNombre() + "')");
				System.out.println(t.getNombre() + " a�adido");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}


	public void consultaTarea(Tareas tar) {
		Statement stat;
		try {
			stat = this.conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT * FROM TAREA WHERE nombre LIKE '"+tar.getNombre()+"%'");
			while(rs.next()) {
				Tareas t = new Tareas(rs.getInt(1),rs.getString(2));
				System.out.println(t.toString());
				consultarDetalle(rs.getInt(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void consultarTareas() {
		// TODO Auto-generated method stub
		Tareas t = new Tareas();
		Statement stat;
		try {
			stat = this.conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT * FROM TAREA");
			while(rs.next()) {
				Tareas tar = new Tareas(rs.getInt(1),rs.getString(2));
				System.out.println(tar.toString());
				consultarDetalle(rs.getInt(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void consultarDetalle(int int1) {
		// TODO Auto-generated method stub
		Statement stat;
		try {
			stat = this.conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT * FROM detalle WHERE ID_tarea="+int1);
			while(rs.next()) {
				Detalle det = new Detalle(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getBoolean(4));
				System.out.println(det.toString());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void altaDetalle(Detalle det) {
		try {
			Statement stat = this.conn.createStatement();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM tarea WHERE ID=?");
			ps.setInt(1, det.getIdTarea());
			ResultSet rs = ps.executeQuery();
			
			//ResultSet rs = stat.executeQuery("SELECT * FROM tarea WHERE ID="+det.getIdTarea());	
			if(rs.getRow() > 0) {
				System.out.println("No se puede insertar el detalle porque la tarea no existe");
			}else {
				stat.executeUpdate("INSERT INTO detalle VALUES(NULL, " + det.getIdTarea() + ",'"+det.getNombreDetalle()+"',"
						+ ""+det.isRealizado()+")");
				System.out.println(det.getNombreDetalle() + " a�adido");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void eliminarTarea(Tareas tar) {
		System.out.println("Se van a borrar todas las lineas de detalle de esta tarea...");
		try {
			Statement stat = this.conn.createStatement();
			stat.executeUpdate("DELETE FROM detalle WHERE ID_tarea="+tar.getIdTarea());
			stat.executeUpdate("DELETE FROM tarea WHERE ID="+tar.getIdTarea());
			//ps.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
